import java.awt.EventQueue;

import java.io.*;
import java.util.*;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class Calculator {

	private JFrame frame;
	private JTextField textField;
	
	private int firstNum;
	private int sum=0;
	private int dif;
	private int prod=1;
	private int imp;
	private Stack<Character> stiva = new Stack<Character>();
	private Vector<String> vect = new Vector<String>();
	private Stack<Integer> stivaC = new Stack<Integer>();
	
	/**
	 * Launch the application.
	 */
	
	public static int prioritate(char operator)
	{
		if(operator == '(' || operator == ')')
			return 0;
		else if(operator == '+' || operator == '-')
			return 1;
		else if (operator == '*' || operator == '/')
			return 2;
		return -1;
	}
	
	public void operatie(int x, int y, String operator, int rez)
	{
		if (operator.equals("+"))
			rez=x+y;
		else if (operator.equals("-"))
			rez = x-y;
		else if(operator.equals("*"))
			rez = x*y;
		else if(operator.equals("/"))
			rez = x/y;
	}
	
	public int calcul()
	{
		int rezultat =  0;
		for(int i=0;i<vect.size();i++)
			{
				try {
				Integer x = Integer.parseInt(vect.get(i));
				if(x!=0)
				{
					stivaC.push(x);
				}
				}catch (NumberFormatException e2) {
					//JOptionPane.showMessageDialog(null, "Not a number");
					//System.out.print(vect.get(i));
					int a = stivaC.pop();
					int b = stivaC.pop();
					System.out.println(a+vect.get(i)+b);
					//System.out.println(vect.get(i));
					operatie(a,b,vect.get(i),rezultat);
					System.out.println(rezultat);
					stivaC.push(rezultat);
					//stivaC.push(operatie(a,b,vect.get(i)));
				}
			}
		return rezultat;
	}
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Calculator window = new Calculator();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Calculator() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 441, 546);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblRezultat = new JLabel("");
		lblRezultat.setBounds(63, 206, 250, 35);
		frame.getContentPane().add(lblRezultat);
		
		JButton button_0 = new JButton("0");
		button_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("0");
				else if(textField.getText().equals("+"))
				{
					textField.setText("0");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("0");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("0");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("0");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10;
						textField.setText(Integer.toString(firstNum));
					}
				
			}
		});
		button_0.setBounds(128, 446, 55, 29);
		frame.getContentPane().add(button_0);
		
		JButton button_2 = new JButton("2");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("2");
				else if(textField.getText().equals("+"))
				{
					textField.setText("2");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("2");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("2");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("2");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 2;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_2.setBounds(128, 407, 55, 29);
		frame.getContentPane().add(button_2);
		
		JButton button_1 = new JButton("1");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("1");
				else if(textField.getText().equals("+"))
				{
					textField.setText("1");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("1");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("1");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("1");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 1;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_1.setBounds(63, 407, 55, 29);
		frame.getContentPane().add(button_1);
		
		JButton button_3 = new JButton("3");
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("3");
				else if(textField.getText().equals("+"))
				{
					textField.setText("3");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("3");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("3");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("3");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 3;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_3.setBounds(193, 407, 55, 29);
		frame.getContentPane().add(button_3);
		
		JButton button_4 = new JButton("4");
		button_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("4");
				else if(textField.getText().equals("+"))
				{
					textField.setText("4");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("4");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("4");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("4");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 4;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_4.setBounds(63, 368, 55, 29);
		frame.getContentPane().add(button_4);
		
		JButton button_5 = new JButton("5");
		button_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("5");
				else if(textField.getText().equals("+"))
				{
					textField.setText("5");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("5");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("5");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("5");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 5;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_5.setBounds(128, 368, 55, 29);
		frame.getContentPane().add(button_5);
		
		JButton button_6 = new JButton("6");
		button_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("6");
				else if(textField.getText().equals("+"))
				{
					textField.setText("6");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("6");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("6");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("6");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 6;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_6.setBounds(193, 368, 55, 29);
		frame.getContentPane().add(button_6);
		
		JButton button_7 = new JButton("7");
		button_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("7");
				else if(textField.getText().equals("+"))
				{
					textField.setText("7");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("7");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("7");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("7");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 7;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_7.setBounds(63, 329, 55, 29);
		frame.getContentPane().add(button_7);
		
		JButton button_8;
		button_8 = new JButton("8");
		button_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("8");
				else if(textField.getText().equals("+"))
				{
					textField.setText("8");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("8");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("8");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("8");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 8;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_8.setBounds(128, 329, 55, 29);
		frame.getContentPane().add(button_8);
		
		JButton button_9 = new JButton("9");
		button_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.getText().equals(""))
					textField.setText("9");
				else if(textField.getText().equals("+"))
				{
					textField.setText("9");
				}
				else if(textField.getText().equals("-"))
				{
					textField.setText("9");
				}
				else if(textField.getText().equals("*"))
				{
					textField.setText("9");
				}
				else if(textField.getText().equals("/"))
				{
					textField.setText("9");
				}
				else
					{
						firstNum=Integer.parseInt(textField.getText())*10 + 9;
						textField.setText(Integer.toString(firstNum));
					}
			}
		});
		button_9.setBounds(193, 329, 55, 29);
		frame.getContentPane().add(button_9);
		
		JButton button_egal = new JButton("=");
		button_egal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				vect.add(Integer.toString(Integer.parseInt(textField.getText())));
				while(!stiva.isEmpty())
					vect.add(String.valueOf(stiva.pop()));
				for(int i=0;i<vect.size();i++)
					System.out.print(vect.get(i));
				lblRezultat.setText(Integer.toString(calcul()));
				System.out.print(calcul());
			}
		});
		button_egal.setBackground(new Color(0, 0, 0));
		button_egal.setForeground(Color.BLUE);
		button_egal.setBounds(258, 446, 55, 29);
		frame.getContentPane().add(button_egal);
		
		JButton button_plus = new JButton("+");
		button_plus.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int numar=Integer.parseInt(textField.getText());
				vect.add(Integer.toString(numar));
				try {
					while(prioritate(stiva.peek()) >= prioritate('+'))
					{
						vect.add(String.valueOf(stiva.pop()));
					}
					stiva.push('+');
					textField.setText("+");
				}catch(EmptyStackException e1)
				{
					stiva.push('+');
					textField.setText("+");
				}
			}
		});
		button_plus.setBounds(258, 407, 55, 29);
		frame.getContentPane().add(button_plus);
	
		JButton button_min = new JButton("-");
		button_min.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int numar = Integer.parseInt(textField.getText());
				vect.add(Integer.toString(numar));
				try {
					while(prioritate(stiva.peek()) >= prioritate('-'))
					{
						vect.add(String.valueOf(stiva.pop()));
					}
					stiva.push('-');
					textField.setText("-");
				}catch(EmptyStackException e1)
				{
					stiva.push('-');
					textField.setText("-");
				}
			}
		});
		button_min.setBounds(258, 368, 55, 29);
		frame.getContentPane().add(button_min);
		
		JButton button_inm = new JButton("*");
		button_inm.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int numar = Integer.parseInt(textField.getText());
				vect.add(Integer.toString(numar));
				try {
					while(prioritate(stiva.peek()) >= prioritate('*'))
					{
						vect.add(String.valueOf(stiva.pop()));
					}
					stiva.push('*');
					textField.setText("*");
				}catch(EmptyStackException e1)
				{
					stiva.push('*');
					textField.setText("*");
				}
			}
		});
		button_inm.setBounds(258, 329, 55, 29);
		frame.getContentPane().add(button_inm);
		
		JButton button_imp = new JButton("/");
		button_imp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int numar = Integer.parseInt(textField.getText());
				vect.add(Integer.toString(numar));
				try {
					while(prioritate(stiva.peek()) >= prioritate('/'))
					{
						vect.add(String.valueOf(stiva.pop()));
					}
					stiva.push('/');
					textField.setText("/");
				}catch(EmptyStackException e1)
				{
					stiva.push('/');
					textField.setText("/");
				}
			}
		});
		button_imp.setBounds(258, 290, 55, 29);
		frame.getContentPane().add(button_imp);
		
		JButton button_14 = new JButton("+/-");
		button_14.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		button_14.setBounds(63, 446, 55, 29);
		frame.getContentPane().add(button_14);
		
		JButton button_15 = new JButton(".");
		button_15.setBounds(193, 446, 55, 29);
		frame.getContentPane().add(button_15);
		
		JButton btnC = new JButton("C");
		btnC.setBounds(63, 290, 55, 29);
		frame.getContentPane().add(btnC);
		
		JButton button_par = new JButton("()");
		button_par.setBounds(128, 290, 55, 29);
		frame.getContentPane().add(button_par);
		
		JButton button_procent = new JButton("%");
		button_procent.setBounds(193, 290, 55, 29);
		frame.getContentPane().add(button_procent);
		
		JButton btnDel = new JButton("DEL");
		btnDel.setBounds(258, 251, 55, 29);
		frame.getContentPane().add(btnDel);
		
		textField = new JTextField();
		textField.setBounds(63, 256, 185, 19);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
	}
}
